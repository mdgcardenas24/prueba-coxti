import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { errorMessage, successDialog } from 'src/app/functions/alerts';
import { Person } from 'src/app/models/person';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm!:FormGroup;
  numberRegEx = /\-?\d*\.?\d{1,2}/;
  person!: Person;

  constructor(private fb:FormBuilder, private authService: AuthService, private router: Router) { 
    this.createForm();
   }

  ngOnInit(): void {
  }

  createForm(): void {
    this.registerForm = this.fb.group({
      fullName: ['', [Validators.required]],
      identification: ['', [Validators.required]],
      cellphone: ['', Validators.compose([Validators.required, Validators.pattern(this.numberRegEx)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', [Validators.required]],
      password2: ['', [Validators.required]],
      department: ['', [Validators.required]],
      city: ['', [Validators.required]],
      neighborhood: ['', [Validators.required]],
      address: ['', [Validators.required]],
      salary: ['', Validators.compose([Validators.required, Validators.pattern(this.numberRegEx)])],
      otherIncome: ['',  Validators.compose([Validators.required, Validators.pattern(this.numberRegEx)])],
      monthlyExpenses: ['',  Validators.compose([Validators.required, Validators.pattern(this.numberRegEx)])],
      financialExpenses: ['',  Validators.compose([Validators.required, Validators.pattern(this.numberRegEx)])]
    })
  }

  register():void{
    if(this.registerForm.invalid) {
      return Object.values(this.registerForm.controls).forEach(control => {
        control.markAsTouched();
      });
    } else {
      this.setPerson();
      this.authService.register(this.person).subscribe((data:any) => {
        successDialog('Registro completado').then(() => {
          this.router.navigate(['/login']);
        });
      }, error => {
        errorMessage(error);
      });
    }
  }

  get fullNameValidate() {
    return (
      this.registerForm.get('fullName')?.invalid && this.registerForm.get('fullName')?.touched
    );
  }

  get identificationValidate() {
    return (
      this.registerForm.get('identification')?.invalid && this.registerForm.get('identification')?.touched
    );
  }

  get cellphoneValidate() {
    return (
      this.registerForm.get('cellphone')?.invalid && this.registerForm.get('cellphone')?.touched
    );
  }

  get emailValidate() {
    return (
      this.registerForm.get('email')?.invalid && this.registerForm.get('email')?.touched
    );
  }

  get departmentValidate() {
    return (
      this.registerForm.get('department')?.invalid && this.registerForm.get('department')?.touched
    );
  }

  get cityValidate() {
    return (
      this.registerForm.get('city')?.invalid && this.registerForm.get('city')?.touched
    );
  }

  get neighborhoodValidate() {
    return (
      this.registerForm.get('neighborhood')?.invalid && this.registerForm.get('neighborhood')?.touched
    );
  }

  get addressValidate() {
    return (
      this.registerForm.get('address')?.invalid && this.registerForm.get('address')?.touched
    );
  }

  get passwordValidate() {
    return (
      this.registerForm.get('password')?.invalid && this.registerForm.get('password')?.touched
    );
  }

  get password2Validate() {
    const pass = this.registerForm.get('password')?.value;
    const pass2 = this.registerForm.get('password2')?.value;
    return pass === pass2 ? false : true;
  }

  get salaryValidate() {
    return (
      this.registerForm.get('salary')?.invalid && this.registerForm.get('salary')?.touched
    );
  }

  get otherIncomeValidate() {
    return (
      this.registerForm.get('otherIncome')?.invalid && this.registerForm.get('otherIncome')?.touched
    );
  }

  get monthlyExpensesValidate() {
    return (
      this.registerForm.get('monthlyExpenses')?.invalid && this.registerForm.get('monthlyExpenses')?.touched
    );
  }
  
  get financialExpensesValidate() {
    return (
      this.registerForm.get('financialExpenses')?.invalid && this.registerForm.get('financialExpenses')?.touched
    );
  }

  setPerson(): void {
    this.person = {
      fullName: this.registerForm.get('fullName')?.value,
      identification: this.registerForm.get('identification')?.value,
      cellphone: this.registerForm.get('cellphone')?.value,
      email: this.registerForm.get('email')?.value,
      password: this.registerForm.get('password')?.value,
      department: this.registerForm.get('department')?.value,
      city: this.registerForm.get('city')?.value,
      neighborhood: this.registerForm.get('neighborhood')?.value,
      address: this.registerForm.get('address')?.value,
      salary: this.registerForm.get('salary')?.value,
      otherIncome: this.registerForm.get('otherIncome')?.value,
      monthlyExpenses: this.registerForm.get('monthlyExpenses')?.value,
      financialExpenses: this.registerForm.get('financialExpenses')?.value
    }
  }
}
