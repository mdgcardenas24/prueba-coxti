import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { errorMessage, successDialog } from 'src/app/functions/alerts';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm!:FormGroup;
  numberRegEx = /\-?\d*\.?\d{1,2}/;
  user!: User;

  constructor(private fb:FormBuilder, private authService: AuthService, private router: Router) { 
    this.createForm();
  }

  ngOnInit(): void {
    
  }

  createForm(): void {
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', [Validators.required]]
    })
  }

  login(): void {
    if(this.loginForm.invalid) {
      return Object.values(this.loginForm.controls).forEach(control => {
        control.markAsTouched();
      });
    } else {
      this.setUser();
      this.authService.login(this.user).subscribe((data:any) => {
        successDialog('Hola de nuevo').then(() => {
          this.authService.token = data.token;
          this.router.navigate(['/home']);
        });
      }, error => {
        errorMessage("Usuario o contraseña incorrecto.");
      });
    }
  }

  get emailValidate() {
    return (
      this.loginForm.get('email')?.invalid && this.loginForm.get('email')?.touched
    );
  }

  get passwordValidate() {
    return (
      this.loginForm.get('password')?.invalid && this.loginForm.get('password')?.touched
    );
  }

  setUser(): void {
    this.user = {
      email: this.loginForm.get('email')?.value,
      password: this.loginForm.get('password')?.value,
    }
  }
}
