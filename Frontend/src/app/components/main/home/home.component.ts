import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { errorMessage } from 'src/app/functions/alerts';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  personForm!:FormGroup;
  constructor(private fb:FormBuilder, private authService: AuthService, private router: Router) { 
    this.personForm = this.fb.group({
      fullName: [{value: '', disabled: true}],
      identification: [{value: '', disabled: true}],
      cellphone: [{value: '', disabled: true}],
      department: [{value: '', disabled: true}],
      city: [{value: '', disabled: true}],
      neighborhood: [{value: '', disabled: true}],
      address: [{value: '', disabled: true}],
      salary: [{value: '', disabled: true}],
      otherIncome: [{value: '', disabled: true}],
      monthlyExpenses: [{value: '', disabled: true}],
      financialExpenses: [{value: '', disabled: true}]
    })
   }

  ngOnInit(): void {
    this.authService.getPerson().subscribe((data:any) => {
      console.log(data[0]);
      this.initForm(data[0]);
    }, error => {
      if(error.status == 401) {
        errorMessage("No estas autorizado para ver esta página");
      } else {
        errorMessage(error.message);
      }
      this.router.navigate(['/login']);
    });
  }

  initForm(person: any) {
    this.personForm = this.fb.group({
      fullName: person.fullname,
      identification: person.identification_code,
      cellphone: person.cellphone,
      department: person.department,
      city: person.city,
      neighborhood: person.neighborhood,
      address: person.address,
      salary: person.salary,
      otherIncome: person.other_income,
      monthlyExpenses: person.monthly_expenses,
      financialExpenses: person.financial_expenses
    })
  }
}
