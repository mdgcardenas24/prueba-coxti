export interface Person {
    fullName: String;
    identification: String;
    cellphone: Number;
    email: String;
    password: String;
    department: String;
    city: String;
    neighborhood: String;
    address: String;
    salary: Number;
    otherIncome: Number;
    monthlyExpenses: Number;
    financialExpenses: Number;
}
