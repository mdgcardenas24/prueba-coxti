import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Person } from '../models/person';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiURL = environment.apiURL;
  token = '';

  constructor(private http: HttpClient) { }

  register(person: Person): Observable<any> {
    return this.http.post(`${this.apiURL}users`, person);
  }

  login(user: User): Observable<any> {
    return this.http.post(`${this.apiURL}login`, user);
  }

  getPerson(): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.token}`
    })
    return this.http.get(`${this.apiURL}getPerson`, { headers: headers });
  }
}
