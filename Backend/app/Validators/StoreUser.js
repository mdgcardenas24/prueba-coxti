'use strict'

const {formatters} = use('Validator')

class StoreUser {
  /**Rules to email and password */
  get rules () {
    return {
      email: 'required|email|max:180|unique:users,email',
      password: 'required|max:100'
    }
  }

  /**Validate all rules */
  get validateAll() {
    return true
  }

  /**Response formater */
  get formatter() {
    return formatters.JsonApi
  }
}

module.exports = StoreUser
