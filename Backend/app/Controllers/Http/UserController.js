'use strict'

const User = use('App/Models/User')
const Person = use('App/Models/Person')
const Database = use('Database')

class UserController {
    /** */
    async store({request, response}) {
        const userData = request.only(['fullName', 'identification', 'cellphone', 'email', 'password', 'department', 'city', 
            'neighborhood', 'address', 'salary', 'otherIncome', 'monthlyExpenses', 'financialExpenses']);
        const user = await User.create({"email": userData.email,"password": userData.password});
        const person = await Person.create({
            'fullname': userData.fullName,
            'identification_code': userData.identification,
            'cellphone': userData.cellphone,
            'department': userData.department,
            'city': userData.city,
            'neighborhood': userData.neighborhood,
            'address': userData.address,
            'salary': userData.salary,
            'other_income': userData.otherIncome,
            'monthly_expenses': userData.monthlyExpenses,
            'financial_expenses': userData.financialExpenses,
            'user_id': user.id
        });
        return response.ok(person);
    }

    async getPerson({auth, response}) {
        const user = await auth.getUser();
        const person = await Database.select('fullname', 'identification_code', 'cellphone', 'department', 'city', 'neighborhood', 'address', 
            'salary', 'other_income', 'monthly_expenses', 'financial_expenses').from('persons').where('user_id', user.id);
        return response.ok(person);
    }
}

module.exports = UserController
