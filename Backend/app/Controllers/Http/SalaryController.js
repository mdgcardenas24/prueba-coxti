'use strict'

class SalaryController {
    async generateSalary({request, response}) {
        const salary = request.only(['salary']);
        var aux = salary.salary;
        var length = 0;
        var max = 1;
        do {
            aux /= 10;
            length += 1;
        } while (aux > 1);
        for(var i = 0; i < length; i++) {
            max *= 10;
        }
        var isFound = false;
        var isInRange = false;
        var randomRange = new Array(3);        ;
        var i = 0;
        var correctOption = 0;
        while(!isFound) {
            var num1 = Math.floor((Math.random() * (max - 0)) + 0);
            var num2 = Math.floor((Math.random() * (max - num1)) + num1);
            i = i == 3 ? 0 : i;
            if(!isInRange) {
                randomRange[i] = {num1, num2};
                if(salary.salary > num1 && salary.salary < num2) {
                    isInRange = true;
                    correctOption = i;
                }
                i++;
            } else {
                if(randomRange[2] != undefined) {
                    isFound = true;
                } else {
                    if(salary.salary < num1 || salary.salary > num2) {
                        randomRange[i] = {num1, num2};
                        i++;
                    }
                }
            }
        }
        const options = ["A", "B", "C"];
        const resp = {
            "a": randomRange[0].num1 + " - " + randomRange[0].num2,
            "b": randomRange[1].num1 + " - " + randomRange[1].num2,
            "c": randomRange[2].num1 + " - " + randomRange[2].num2,
            "Opción correcta": options[correctOption]
        };
        return response.ok(resp);
    }
}

module.exports = SalaryController
