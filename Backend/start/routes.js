'use strict'

const Route = use('Route')

Route.post('/login','AuthController.login')
Route.resource('users','UserController').apiOnly().validator(new Map([[['users.store'],['StoreUser']]]))
Route.get('/getPerson','UserController.getPerson').middleware('auth');
Route.post('/salary', 'SalaryController.generateSalary')
