'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PersonsSchema extends Schema {
  up () {
    this.create('persons', (table) => {
      table.increments()
      table.text('fullname').notNullable()
      table.string('identification_code').notNullable()
      table.string('cellphone').notNullable()
      table.string('department').notNullable()
      table.string('city').notNullable()
      table.string('neighborhood').notNullable()
      table.text('address').notNullable()
      table.string('salary').notNullable()
      table.string('other_income').notNullable()
      table.string('monthly_expenses').notNullable()
      table.string('financial_expenses').notNullable()
      table.timestamps()
      table.integer('user_id').unsigned().references('id').inTable('users')
    })
  }

  down () {
    this.drop('persons')
  }
}

module.exports = PersonsSchema
